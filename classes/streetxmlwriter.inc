<?php

/**
 * @file
 * UPSAddressXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * UPS Shipping XML API Developers Guide, 2 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML address validation request.
 *
 * Code complies with XML Schema documented in the
 * UPS Address Validation XML Developers Guide, 1 January 2012.
 *
 * @param ShippingAddressInterface $address
 *   The order id of the shipment.
 */
class UPSAddressXMLWriter extends UPSXMLWriter {

  /**
   * Address to validate.
   *
   * @var ShippingAddressInterface
   */
  protected $address;


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for address property.
   */
  public function setAddress(ShippingAddressInterface $address) {
    $this->address = $address;
    return $this;
  }

  /**
   * Overrides UPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL() . 'XAV';
  }

  /**
   * Writes UPS AddressValidationRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function addressValidationRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('AddressValidationRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->writeElement(    'RequestAction', 'XAV');
    $this->writeElement(    'RequestOption', '3');
    $this->writeElement(    'TransactionReference');
    //$this->startElement(    'TransactionReference');
    //$this->writeElement(      'CustomerContext', 'any important info');
    //$this->endElement();//   TransactionReference
    $this->endElement();// Request
    $this->startElement(  'AddressKeyFormat');
    //$this->writeElement(    'AddressLine', $this->address->getStreet1());
    //$this->writeElement(    'AddressLine', $this->address->getStreet2());
    $this->writeElement(    'PoliticalDivision2', $this->address->getCity());
    $this->writeElement(    'PoliticalDivision1', $this->address->getZone());
    $this->writeElement(    'PostcodePrimaryLow', substr($this->address->getPostalCode(), 0, 5));
    $this->writeElement(    'CountryCode', $this->address->getCountry());
    $this->endElement();// AddressKeyFormat

    $this->endElement();// AddressValidationRequest
    $this->endDocument();

    return $this;
  }

}

<?php

/**
 * @file
 * UPSTransitXMLWriter class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML address validation request.
 *
 * Code complies with XML Schema documented in the
 * UPS Time In Transit - Package XML Developers Guide, 1 January 2012.
 *
 * @param ShippingAddressInterface $address
 *   The order id of the shipment.
 *
 * @return
 *   AddressValidationRequest XML document to send to UPS.
 */
class UPSTransitXMLWriter extends UPSXMLWriter {

  /**
   * Address to validate.
   *
   * @var ShippingAddressInterface
   */
  protected $address;


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for address property.
   */
  public function setAddress(ShippingAddressInterface $address) {
    $this->address = $address;
    return $this;
  }

  /**
   * Accessor for service URL.
   */
  public function getURL() {
    return parent::getBaseURL() . 'AV';
  }

  /**
   * Writes UPS TimeInTransitRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function transitTimeRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('TimeInTransitRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->writeElement(    'RequestAction', 'TimeInTransit');
    $this->writeElement(    'TransactionReference');
    //$this->startElement(    'TransactionReference');
    //$this->writeElement(      'CustomerContext', 'any important info');
    //$this->endElement();//   TransactionReference
    $this->endElement();// Request
    $this->startElement(  'TransitFrom');
    $this->startElement(    'AddressArtifactFormat');
    $this->writeElement(      'PoliticalDivision2', $this->address->getCity());
    $this->writeElement(      'PoliticalDivision1', $this->address->getZone());
    $this->writeElement(      'PostcodePrimaryHigh', $this->address->getPostalCode());
    $this->writeElement(      'CountryCode', $this->address->getCountry());
    $this->endElement();//   AddressArtifactFormat
    $this->endElement();// TransitFrom
    $this->startElement(  'TransitTo');
    $this->startElement(    'AddressArtifactFormat');
    $this->writeElement(      'PoliticalDivision2', $this->address->getCity());
    $this->writeElement(      'PoliticalDivision1', $this->address->getZone());
    $this->writeElement(      'PostcodePrimaryHigh', $this->address->getPostalCode());
    $this->writeElement(      'CountryCode', $this->address->getCountry());
    $this->endElement();//   AddressArtifactFormat
    $this->endElement();// TransitTo
    $this->startElement(  'ShipmentWeight');
    $this->startElement(    'UnitOfMeasurement');
    $this->writeElement(      'Code', 'LBS');
    $this->writeElement(      'Description', 'Pounds');
    $this->endElement();//   UnitOfMeasurement
    $this->writeElement(    'Weight', number_format($shipment_weight, 1, '.', ''));
    $this->endElement();// ShipmentWeight


    $this->endElement();// TimeInTransitRequest
    $this->endDocument();

    return $this;
  }

}

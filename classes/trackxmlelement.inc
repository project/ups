<?php
/**
 * @file
 * Defines a SimpleXMLElement subclass specific to UPS Tracking responses.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Provides an interface to access XML containing UPS Tracking responses.
 */
class UPSTrackingXMLElement extends SimpleXMLElement implements ShippingTrackingInterface {

  /**
   * Implements ShippingTrackingInterface::getCarrier().
   */
  public function getCarrier() {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();
    return (string) 'UPS';
  }

  /**
   * Implements ShippingTrackingInterface::getTrackingNumber().
   */
  public function getTrackingNumbers() {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $tracking_numbers = array();
    $tracking_numbers[] = (string) $this->Shipment->Package->TrackingNumber;


    return $tracking_numbers;
  }

  /**
   * Implements ShippingTrackingInterface::getStatus().
   */
  public function getStatus($tracking_number) {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    return (string) $this->Shipment->Package->Activity[0]->Status->StatusType->Description;
  }

  /**
   * Extracts package tracking information from response XML.
   *
   * @return
   *   An array of ShippingTrackingEvent objects.
   */
  public function getTrackingInfo($tracking_number) {
    // Check for invalid or failed request before parsing response.
    $this->checkForErrorResponse();

    $tracking_info = array();

    if (isset($info->Error)) {
      // This shouldn't be an Exception, as it's an expected and recoverable
      // situation.
      throw new ShippingTrackingException(t('UPS has no record of this item. Please verify your information and try again later. Error :error: - ":description".', array(':error' => $info->Error->Number, ':description' => $info->Error->Description)));
    }

    // Current status is stored in TrackSummary elements,
    // Intermediate status(es) are stored in TrackDetail elements.

    foreach ($this->Shipment->Package->Activity as $activity) {
      // Start a new event.
      $event = new ShippingTrackingEvent();
      $event->setTime(format_date(strtotime($activity->Time), 'custom', 'g:i a'));
      $event->setDate(format_date(strtotime($activity->Date), 'custom', 'F j, Y'));
      $event->setEvent((string) $activity->Status->StatusType->Description);

      $location = '';
      $address = $activity->ActivityLocation->Address;
      if (!empty($address->City)) {
        $location  = $address->City;
        if (!empty($address->StateProvinceCode)) {
          $location .= ', ' . $address->StateProvinceCode;
          if (!empty($address->CountryCode)) {
            $location .= ', ' . $address->CountryCode;
          }
        }
      }
      $event->setLocation((string) $location);
      // Save event for return.
      $tracking_info[] = $event;
    }

    return $tracking_info;

    // No tracking info found for this tracking number.
    throw new ShippingTrackingException(t('UPS response does not contain information about tracking number :number.', array(':number' => $tracking_number)));
  }

  /**
   * Checks XML Response for errors.
   */
  protected function checkForErrorResponse() {
    // Check for invalid or failed tracking request.
    if (isset($this->Response->Error)) {
      throw new ShippingTrackingException(t('UPS could not locate the shipment details for your request. Please verify your information and try again later.'), 'error');
    }
  }

}

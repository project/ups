<?php

/**
 * @file
 * UPSShipmentAcceptXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * UPS Shipping XML API Developers Guide, 2 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML label and pickup request.
 *
 * @param $digest
 *   Base-64 encoded shipment request.
 * @param $order_id
 *   The order id of the shipment.
 * @param $packages
 *   An array of package ids to be shipped.
 */
class UPSShipmentAcceptXMLWriter extends UPSXMLWriter {

  /**
   * Origin address for shipment.
   *
   * @var ShippingAddressInterface
   */
  protected $customer_context = '';

  /**
   * Origin address for shipment.
   *
   * @var string
   */
  protected $shipping_identification_number = '';

  /**
   * Destination address for shipment.
   *
   * @var string
   */
  protected $tracking_number = '';


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for customer_context property.
   */
  public function setCustomerContext($customer_context) {
    $this->customer_context = $customer_context;
    return $this;
  }

  /**
   * Accessor for customer_context property.
   */
  public function getCustomerContext($customer_context) {
    return $this->customer_context;
  }

  /**
   * Mutator for shipment_identification_number property.
   */
  public function setShipmentIdentificationNumber($shipment_identification_number) {
    $this->shipment_identification_number = $shipment_identification_number;
    return $this;
  }

  /**
   * Accessor for shipment_identification_number property.
   */
  public function getShipmentIdentificationNumber($shipment_identification_number) {
    return $this->shipment_identification_number;
  }

  /**
   * Mutator for tracking_number property.
   */
  public function setTrackingNumber($tracking_number) {
    $this->tracking_number = $tracking_number;
    return $this;
  }

  /**
   * Accessor for tracking_number property.
   */
  public function getTrackingNumber($tracking_number) {
    return $this->tracking_number;
  }

  /**
   * Overrides UPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL() . 'ShipAccept';
  }

  /**
  /**
   * Writes UPS ShipmentAcceptRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function shipmentAcceptRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('ShipmentAcceptRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->writeElement(    'RequestAction', 'ShipAccept');
    $this->startElement(    'TransactionReference');
    $this->writeElement(      'OrderId', $this->order_id);
    $this->writeElement(      'PackageId', $this->package_id);
    $this->writeElement(      'CustomerContext', $this->customer_context);
//      $schema .= t('Void shipment @ship_number and tracking numbers @track_list', array('@ship_number' => $shipment_number, '@track_list' => implode(', ', $tracking_numbers)));
    $this->endElement();//   TransactionReference
    $this->endElement();// Request
    $this->writeElement(   'ShipmentDigest', $digest);

    $this->endElement();// ShipmentAcceptRequest
    $this->endDocument();

    return $this;
  }

}

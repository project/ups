<?php

/**
 * @file
 * UPSVoidXMLWriter class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 *
 */
class UPSVoidXMLWriter extends UPSXMLWriter {

  /**
   * Origin address for shipment.
   *
   * @var string
   */
  protected $customer_context = '';

  /**
   * Origin address for shipment.
   *
   * @var string
   */
  protected $shipping_identification_number = '';

  /**
   * Destination address for shipment.
   *
   * @var string
   */
  protected $tracking_number = '';


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for customer_context property.
   */
  public function setCustomerContext($customer_context) {
    $this->customer_context = $customer_context;
    return $this;
  }

  /**
   * Accessor for customer_context property.
   */
  public function getCustomerContext($customer_context) {
    return $this->customer_context;
  }

  /**
   * Mutator for shipment_identification_number property.
   */
  public function setShipmentIdentificationNumber($shipment_identification_number) {
    $this->shipment_identification_number = $shipment_identification_number;
    return $this;
  }

  /**
   * Accessor for shipment_identification_number property.
   */
  public function getShipmentIdentificationNumber($shipment_identification_number) {
    return $this->shipment_identification_number;
  }

  /**
   * Mutator for tracking_number property.
   */
  public function setTrackingNumber($tracking_number) {
    $this->tracking_number = $tracking_number;
    return $this;
  }

  /**
   * Accessor for tracking_number property.
   */
  public function getTrackingNumber($tracking_number) {
    return $this->tracking_number;
  }

  /**
   * Overrides UPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL() . 'Void';
  }

  /**
  /**
   * Writes UPS VoidShipmentRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function voidShipmentRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('VoidShipmentRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->startElement(    'TransactionReference');
    $this->writeElement(      'CustomerContext', $this->customer_context);
//      $schema .= t('Void shipment @ship_number and tracking numbers @track_list', array('@ship_number' => $shipment_number, '@track_list' => implode(', ', $tracking_numbers)));
    $this->endElement();//   TransactionReference
    $this->endElement();// Request

    $this->startElement(  'ExpandedVoidShipment');
    $this->writeElement(    'ShipmentIdentificationNumber', $this->shipment_identification_number);
    $this->writeElement(    'TrackingNumber', $this->tracking_number);
    $this->endElement();// ExpandedVoidShipment
    $this->endElement();// VoidShipmentRequest
    $this->endDocument();

    return $this;
  }

}

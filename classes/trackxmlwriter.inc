<?php

/**
 * @file
 * UPSTrackXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * UPS Tracking XML Developers Guide, 1 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML tracking request.
 *
 * @param $tracking_number
 *   The order id of the shipment.
 *
 * @return
 *   TrackRequest XML document to send to UPS.
 */
class UPSTrackXMLWriter extends UPSXMLWriter {

  /**
   * Tracking number for package.
   *
   * @var string
   */
  protected $tracking_number = '';


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for tracking_number property.
   */
  public function setTrackingNumber($tracking_number) {
    $this->tracking_number = $tracking_number;
    return $this;
  }

  /**
   * Accessor for tracking_number property.
   */
  public function getTrackingNumber($tracking_number) {
    return $this->tracking_number;
  }

  /**
   * Overrides UPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL() . 'Track';
  }

  /**
  /**
   * Writes UPS TrackRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function trackRequest() {
    // UPS only allows 1 tracking number per request.
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('TrackRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->writeElement(    'RequestAction', 'Track');
    $this->writeElement(    'RequestOption', 'activity');
    $this->startElement(    'TransactionReference');
    $this->writeElement(      'CustomerContext', 'any important info');
    $this->endElement();//   TransactionReference
    $this->endElement();// Request
    $this->writeElement(  'TrackingNumber', $this->tracking_number);
    $this->writeElement(  'IncludeFreight', '02'); // 02 = small package only
                                                   // 01 = small package + freight

    $this->endElement();// TrackRequest
    $this->endDocument();

    return $this;
  }

}

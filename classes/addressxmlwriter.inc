<?php

/**
 * @file
 * UPSAddressXMLWriter class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML address validation request.
 *
 * Code complies with XML Schema documented in the
 * UPS Address Validation XML Developers Guide, 1 January 2012.
 *
 * @param ShippingAddressInterface $address
 *   The order id of the shipment.
 *
 * @return
 *   AddressValidationRequest XML document to send to UPS.
 */
class UPSAddressXMLWriter extends UPSXMLWriter {

  /**
   * Address to validate.
   *
   * @var ShippingAddressInterface
   */
  protected $address;


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for address property.
   */
  public function setAddress(ShippingAddressInterface $address) {
    $this->address = $address;
    return $this;
  }

  /**
   * Accessor for service URL.
   */
  public function getURL() {
    return parent::getBaseURL() . 'AV';
  }

  /**
   * Writes UPS AddressValidationRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function addressValidationRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('AddressValidationRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->writeElement(    'RequestAction', 'AV');
    $this->writeElement(    'TransactionReference');
    //$this->startElement(    'TransactionReference');
    //$this->writeElement(      'CustomerContext', 'any important info');
    //$this->endElement();//   TransactionReference
    $this->endElement();// Request
    $this->startElement(  'Address');
    $this->writeElement(    'City', $this->address->getCity());
    $this->writeElement(    'StateProvinceCode', $this->address->getZone());
    $this->writeElement(    'PostalCode', $this->address->getPostalCode());
    $this->writeElement(    'CountryCode', $this->address->getCountry());
    $this->endElement();// Address

    $this->endElement();// AddressValidationRequest
    $this->endDocument();

    return $this;
  }

}

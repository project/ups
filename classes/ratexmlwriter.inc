<?php

/**
 * @file
 * UPSRateXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * UPS Rating Package XML Developers Guide, 2 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 *
 */
class UPSRateXMLWriter extends UPSXMLWriter {

  /**
   * Origin address for shipment.
   *
   * @var ShippingAddressInterface
   */
  protected $from;

  /**
   * Destination address for shipment.
   *
   * @var ShippingAddressInterface
   */
  protected $to;

  /**
   * Array of PackagingPackage objects.
   *
   * @var array
   */
  protected $packages = array();


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for origin address.
   */
  public function setShipFromAddress(ShippingAddressInterface $from) {
    $this->from = $from;
    return $this;
  }

  /**
   * Mutator for destination address.
   */
  public function setShipToAddress(ShippingAddressInterface $to) {
    $this->to = $to;
    return $this;
  }

  /**
   * Mutator for packages property.
   */
  public function setPackages(array $packages) {
    $this->packages = $packages;
    return $this;
  }

  /**
   * Overrides UPSXMLWriter::getURL().
   */
  public function getURL() {
    return parent::getBaseURL() . 'Rate';
  }

  /**
   * Writes UPS RatingServiceSelectionRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function rateRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('RatingServiceSelectionRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->startElement(    'TransactionReference');
    $this->writeElement(      'CustomerContext', 'Complex Rate Request');
    $this->endElement();//   TransactionReference
    $this->writeElement(    'RequestAction', 'Rate');
    $this->writeElement(    'RequestOption', 'Rate'); // For a specific service
    //$this->writeElement(    'RequestOption', 'Shop'); // For ALL services!
    $this->endElement();// Request
    $this->startElement(  'PickupType');
    $this->writeElement(    'Code', variable_get('ups_pickup_type', '01'));
    $this->endElement();
    $this->startElement(  'CustomerClassification');
    $this->writeElement(    'Code', variable_get('ups_classification', '04'));
    $this->endElement();
    $this->startElement(  'Shipment');
    $this->startElement(    'Shipper');
    $this->writeElement(      'ShipperNumber', $this->credentials->getShipperNumber());
    $this->startElement(      'Address');
    $this->writeElement(        'AddressLine1', $this->from->getStreet1());
    $this->writeElement(        'AddressLine2', $this->from->getStreet2());
    $this->writeElement(        'City', $this->from->getCity());
    $this->writeElement(        'StateProvinceCode', $this->from->getZone());
    $this->writeElement(        'PostalCode', $this->from->getPostalCode());
    $this->writeElement(        'CountryCode', $this->from->getCountry());
    $this->endElement();//     Address
    $this->endElement();//   Shipper

    $this->startElement(    'ShipTo');
    $this->startElement(      'Address');
    $this->writeElement(        'AddressLine1', $this->to->getStreet1());
    $this->writeElement(        'AddressLine2', $this->to->getStreet2());
    $this->writeElement(        'City', $this->to->getCity());
    $this->writeElement(        'StateProvinceCode', $this->to->getZone());
    $this->writeElement(        'PostalCode', $this->to->getPostalCode());
    $this->writeElement(        'CountryCode', $this->to->getCountry());
    if (variable_get('ups_residential_quotes', 0)) {
      $this->writeElement(      'ResidentialAddressIndicator');
    }
    $this->endElement();//     Address
    $this->endElement();//   ShipTo
    $this->startElement(    'ShipFrom');
    $this->startElement(      'Address');
    $this->writeElement(        'AddressLine1', $this->from->getStreet1());
    $this->writeElement(        'AddressLine2', $this->from->getStreet2());
    $this->writeElement(        'City', $this->from->getCity());
    $this->writeElement(        'StateProvinceCode', $this->from->getZone());
    $this->writeElement(        'PostalCode', $this->from->getPostalCode());
    $this->writeElement(        'CountryCode', $this->from->getCountry());
    $this->endElement();//     Address
    $this->endElement();//   ShipFrom

    $this->startElement(    'Service');
    $list = ups_service_list();
    $code = array_rand($list);
    $this->writeElement(      'Code', $code);
    $this->writeElement(      'Description', $list[$code]);
    $this->endElement();//   Service

    // Loop over Packages here
    $shipment_weight = 0;
    foreach ($this->packages as $package) {

      $this->startElement(  'Package');

    $plist = ups_package_types();
    $pcode = array_rand($plist);
      $this->startElement(    'PackagingType');
      $this->writeElement(      'Code', $pcode);
      $this->endElement();//   PackagingType

      $this->startElement(    'Dimensions');
      $this->startElement(      'UnitOfMeasurement');
      $this->writeElement(        'Code', $package->getLengthUnits());
      $this->endElement();//     UnitOfMeasurement
      $this->writeElement(      'Length', '1');
      $this->writeElement(      'Width',  '1');
      $this->writeElement(      'Height', '1');
      $this->endElement();//   Dimensions

      $this->startElement(    'PackageWeight');
      $this->startElement(      'UnitOfMeasurement');
      $this->writeElement(        'Code', strtoupper($package->getWeightUnits()));
      $this->writeElement(        'Description', 'Pounds');
      $this->endElement();//     UnitOfMeasurement
      $this->writeElement(      'Weight', number_format($package->getShipWeight(), 1, '.', ''));
      $this->endElement();//   PackageWeight
      $shipment_weight += $package->getShipWeight();

      //if ($size > 130 && $size <= 165) {
      //  $this->writeElement('LargePackageIndicator');
      //}

      if (variable_get('ups_insurance', TRUE)) {
        $this->startElement(  'PackageServiceOptions');
        $this->startElement(    'InsuredValue');
        $this->writeElement(      'CurrencyCode', $package->getCurrencyCode());
        $this->writeElement(      'MonetaryValue', $package->getPrice());
        $this->endElement();//   InsuredValue
        $this->endElement();// PackageServiceOptions
      }
      $this->endElement();// Package

    } // End loop over packages

    // Shipment summary
    $this->startElement(    'ShipmentWeight');
    $this->startElement(      'UnitOfMeasurement');
    $this->writeElement(        'Code', 'LBS');
    $this->writeElement(        'Description', 'Pounds');
    $this->endElement();//     UnitOfMeasurement
    $this->writeElement(      'Weight', number_format($shipment_weight, 1, '.', ''));
    $this->endElement();//   ShipmentWeight

    if (variable_get('ups_negotiated_rates', FALSE)) {
      $this->startElement( 'RateInformation');
      $this->writeElement(   'NegotiatedRatesIndicator');
      $this->endElement();//RateInformation
    }

    $this->endElement();//Shipment
    $this->endElement();//RatingServiceSelectionRequest
    $this->endDocument();

    return $this;
  }
}

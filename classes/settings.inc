<?php

/**
 * @file
 * UPSSettings utility class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Class for UPS settings.
 */
class UPSSettings {

  protected $residential_quotes = TRUE;
  protected $pickup_type = '01';
  protected $shipper_classification = '04';
  protected $shipper_number = '';
  protected $negotiated_rates = FALSE;


  /**
   * Mutator for residential_quotes property.
   */
  public function setResidentialQuotes($residential_quotes) {
    $this->residential_quotes = $residential_quotes;
    return $this;
  }

  /**
   * Accessor for residential_quotes property.
   */
  public function isResidentialQuotes() {
    return $this->residential_quotes;
  }

  /**
   * Mutator for pickup_type property.
   */
  public function setPickupType($pickup_type) {
    $this->pickup_type = $pickup_type;
    return $this;
  }

  /**
   * Accessor for pickup_type property.
   */
  public function getPickupType() {
    return $this->pickup_type;
  }

  /**
   * Mutator for shipper_classification property.
   */
  public function setShipperClassification($shipper_classification) {
    $this->shipper_classification = $shipper_classification;
    return $this;
  }

  /**
   * Accessor for shipper_classification property.
   */
  public function getShipperClassification() {
    return $this->shipper_classification;
  }

  /**
   * Mutator for shipper_number property.
   */
  public function setShipperNumber($shipper_number) {
    $this->shipper_number = $shipper_number;
    return $this;
  }

  /**
   * Accessor for shipper_number property.
   */
  public function getShipperNumber() {
    return $this->shipper_number;
  }

  /**
   * Mutator for negotiated_rates property.
   */
  public function setNegotiatedRates($negotiated_rates) {
    $this->negotiated_rates = $negotiated_rates;
    return $this;
  }

  /**
   * Accessor for negotiated_rates property.
   */
  public function isNegotiatedRates() {
    return $this->negotiated_rates;
  }

}

<?php

/**
 * @file
 * UPSFreightRateXMLWriter class definition.
 *
 * Code complies with XML Schema documented in the
 * UPS Rating Package XML Developers Guide, 2 January 2012.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Constructs an XML freight rate request.
 *
 * Code complies with XML Schema documented in the
 * UPS Address Validation XML Developers Guide, 1 January 2012.
 */
class UPSFreightRateXMLWriter extends UPSXMLWriter {

  /**
   * Origin address for shipment.
   *
   * @var ShippingAddressInterface
   */
  protected $from;

  /**
   * Destination address for shipment.
   *
   * @var ShippingAddressInterface
   */
  protected $to;

  /**
   * Array of PackagingPackage objects.
   *
   * @var array
   */
  protected $packages = array();


  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Mutator for origin address.
   */
  public function setShipFromAddress(ShippingAddressInterface $from) {
    $this->from = $from;
    return $this;
  }

  /**
   * Mutator for destination address.
   */
  public function setShipToAddress(ShippingAddressInterface $to) {
    $this->to = $to;
    return $this;
  }

  /**
   * Mutator for packages property.
   */
  public function setPackages(array $packages) {
    $this->packages = $packages;
    return $this;
  }

  /**
   * Overrides UPSXMLWriter::getURL().
   */
  public function getURL() {
    // Freight rates require different base URL path.
    return str_replace(
      array('ups.app/xml'),
      array('webservices'),
      parent::getBaseURL()
    ) . 'FreightRate';
  }

  /**
   * Writes UPS RatingServiceSelectionRequest XML into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  public function freightRequest() {
    $this->writeHeader();
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('FreightRateRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->startElement(  'Request');
    $this->startElement(    'TransactionReference');
    $this->writeElement(      'CustomerContext', 'any information here');
    $this->endElement();//   TransactionReference
    $this->writeElement(    'RequestOption', '1'); // '1' if by Ground,
                                                   // '2' if by Air. Need const.
    $this->endElement();// Request

    $this->startElement(  'ShipFrom');
    $this->writeElement(    'Name', $this->from->getCompany());
    $this->writeElement(    'AttentionName', $this->from->getName());
    $this->startElement(    'Address');
    $this->writeElement(      'AddressLine', $this->from->getStreet1());
    $this->writeElement(      'AddressLine', $this->from->getStreet2());
    $this->writeElement(      'City', $this->from->getCity());
    $this->writeElement(      'StateProvinceCode', $this->from->getZone());
    $this->writeElement(      'PostalCode', $this->from->getPostalCode());
    $this->writeElement(      'CountryCode', $this->from->getCountry());
    $this->endElement();//   Address
    $this->endElement();// ShipFrom

    $this->startElement(  'ShipTo');
    $this->writeElement(    'Name', $this->to->getCompany());
    $this->writeElement(    'AttentionName', $this->to->getName());
    $this->startElement(    'Address');
    $this->writeElement(      'AddressLine', $this->to->getStreet1());
    $this->writeElement(      'AddressLine', $this->to->getStreet2());
    $this->writeElement(      'City', $this->to->getCity());
    $this->writeElement(      'StateProvinceCode', $this->to->getZone());
    $this->writeElement(      'PostalCode', $this->to->getPostalCode());
    $this->writeElement(      'CountryCode', $this->to->getCountry());
    $this->endElement();//     Address
    $this->endElement();//   ShipTo

    $this->startElement(  'PaymentInformation');
    $this->startElement(    'Payer');
    $this->writeElement(      'Name', $this->from->getCompany());
    $this->writeElement(      'AttentionName', $this->from->getName());
    $this->writeElement(      'ShipperNumber', $this->credentials->getShipperNumber());
    $this->startElement(      'Address');
    $this->writeElement(        'AddressLine', $this->from->getStreet1());
    $this->writeElement(        'AddressLine', $this->from->getStreet2());
    $this->writeElement(        'City', $this->from->getCity());
    $this->writeElement(        'StateProvinceCode', $this->from->getZone());
    $this->writeElement(        'PostalCode', $this->from->getPostalCode());
    $this->writeElement(        'CountryCode', $this->from->getCountry());
    $this->endElement();//     Address
    $this->endElement();//   Payer
    $this->endElement();// PaymentInformation

    $this->startElement(  'Service');
    $list = ups_service_list();
    $code = array_rand($list);
    $this->writeElement(    'Code', $code);
    $this->writeElement(    'Description', $list[$code]);
    $this->endElement();// Service

    // Loop over Packages here
    $shipment_weight = 0;
    $id = 0;
    foreach ($this->packages as $package) {

      $this->startElement(  'Commodity');
      $this->writeElement(    'CommodityID', $id++);
      $this->writeElement(    'Description', 'what this is');

      $this->startElement(    'Weight');
      $this->startElement(      'UnitOfMeasurement');
      $this->writeElement(        'Code', strtoupper($package->getWeightUnits()));
      $this->writeElement(        'Description', 'Pounds');
      $this->endElement();//     UnitOfMeasurement
      $this->writeElement(      'Value', number_format($package->getShipWeight(), 1, '.', ''));
      $this->endElement();//   Weight
      $shipment_weight += $package->getShipWeight();

      $this->startElement(    'Dimensions');
      $this->startElement(      'UnitOfMeasurement');
      $this->writeElement(        'Code', $package->getLengthUnits());
      $this->endElement();//     UnitOfMeasurement
      $this->writeElement(      'Length', '1');
      $this->writeElement(      'Width',  '1');
      $this->writeElement(      'Height', '1');
      $this->endElement();//   Dimensions

    $plist = ups_package_types();
    $pcode = array_rand($plist);
      $this->startElement(    'PackagingType');
      $this->writeElement(      'Code', $pcode);
      $this->endElement();//   PackagingType

      //if ($size > 130 && $size <= 165) {
      //  $this->writeElement('LargePackageIndicator');
      //}

      if (variable_get('ups_insurance', TRUE)) {
        $this->startElement(  'PackageServiceOptions');
        $this->startElement(    'InsuredValue');
        $this->writeElement(      'CurrencyCode', $package->getCurrencyCode());
        $this->writeElement(      'MonetaryValue', $package->getPrice());
        $this->endElement();//   InsuredValue
        $this->endElement();// PackageServiceOptions
      }
      $this->endElement();// Package

    } // End loop over packages

    // Shipment summary
    $this->startElement(    'ShipmentTotalWeight');
    $this->startElement(      'UnitOfMeasurement');
    $this->writeElement(        'Code', 'LBS');
    $this->writeElement(        'Description', 'Pounds');
    $this->endElement();//     UnitOfMeasurement
    $this->writeElement(      'Value', number_format($shipment_weight, 1, '.', ''));
    $this->endElement();//   ShipmentWeight

    $this->endElement();//FreightRateRequest
    $this->endDocument();

    return $this;
  }
}

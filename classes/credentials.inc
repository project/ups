<?php

/**
 * @file
 * UPSCredentials utility class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 * Class for UPS credentials.
 */
class UPSCredentials {

  private $userid = '';
  private $password = '';
  private $accesslicensenumber = '';
  private $shippernumber = '';

  /**
   * Constructor.
   *
   * @param array $credentials
   *   - userid:
   *   - password:
   *   - accesslicensenumber:
   *   - shippernumber:
   */
  public function __construct(array $credentials = array()) {
    // Defaults for unset keys.
    $credentials += array(
      'userid'              => '',
      'password'            => '',
      'accesslicensenumber' => '',
      'shippernumber'       => '',
    );

    $this->userid              = $credentials['userid'];
    $this->password            = $credentials['password'];
    $this->accesslicensenumber = $credentials['accesslicensenumber'];
    $this->shippernumber       = $credentials['shippernumber'];
  }

  /**
   * Mutator for userid property.
   */
  public function setUserID($userid) {
    $this->userid = $userid;
    return $this;
  }

  /**
   * Accessor for userid property.
   */
  public function getUserID() {
    return $this->userid;
  }

  /**
   * Mutator for password property.
   */
  public function setPassword($password) {
    $this->password = $password;
    return $this;
  }

  /**
   * Accessor for password property.
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * Mutator for accesslicensenumber property.
   */
  public function setAccessLicenseNumber($accesslicensenumber) {
    $this->accesslicensenumber = $accesslicensenumber;
    return $this;
  }

  /**
   * Accessor for accesslicensenumber property.
   */
  public function getAccessLicenseNumber() {
    return $this->accesslicensenumber;
  }

  /**
   * Mutator for shippernumber property.
   */
  public function setShipperNumber($shippernumber) {
    $this->shippernumber = $shippernumber;
    return $this;
  }

  /**
   * Accessor for shippernumber property.
   */
  public function getShipperNumber() {
    return $this->shippernumber;
  }
}

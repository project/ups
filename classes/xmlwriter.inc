<?php

/**
 * @file
 * UPSXMLWriter abstract base class definition.
 *
 * @author Tim Rohaly.    <http://drupal.org/user/202830>
 */


/**
 *
 */
abstract class UPSXMLWriter extends XMLWriter {

  /**
   * Shipper's UPS API credentials.
   *
   * @var UPSCredentials
   */
  protected $credentials;

  /** Flag for testing service mode. */
  const TESTING    = 0;

  /** Flag for production service mode. */
  const PRODUCTION = 1;

  /**
   * Base URL for testing service mode.
   *
   * @var string
   */
  protected static $testing_base_url = 'https://wwwcie.ups.com/ups.app/xml/';

  /**
   * Base URL for production service mode.
   *
   * @var string
   */
  protected static $production_base_url = 'https://onlinetools.ups.com/ups.app/xml/';

  /**
   * Default to TESTING service mode.
   *
   * @var int
   */
  protected $service_mode = UPSXMLWriter::TESTING;

  /**
   * Debug mode for output will pretty-print the XML.
   *
   * @var bool
   */
  protected $debug = FALSE;


  /**
   * Constructor.
   */
  public function __construct() {
    //parent::__construct();
    $this->openMemory();
  }

  /**
   * Mutator for credentials property.
   *
   * @param UPSCredentials $credentials
   *   An associative array containing the following keys:
   *   -accesslicensenumber:
   *   -userid:
   *   -password:
   */
  public function setCredentials(UPSCredentials $credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * Mutator for service_mode property.
   */
  public function setServiceMode($service_mode) {
// need to validate that this is either TESTING or PRODUCTION
    $this->service_mode= $service_mode;
    return $this;
  }

  /**
   * Accessor for base service URL.
   */
  protected function getBaseURL() {
    switch ($this->service_mode) {
      case UPSXMLWriter::PRODUCTION:
        return self::$production_base_url;
        break;

      case UPSXMLWriter::TESTING:
      default:
        return self::$testing_base_url;
        break;
    }
  }

  /**
   * Mutator for debug property.
   *
   * @param $debug
   *   TRUE to pretty-print the XML (line breaks and indentations).
   */
  public function setDebug($debug = FALSE) {
    $this->debug = $debug;
    $this->setIndent($debug);
    return $this;
  }

  /**
   * Accessor for debug property.
   */
  public function isDebug() {
    return $this->debug;
  }

  /**
   * Accessor for service URL.
   *
   * Subclasses must implement this. The typical implementation should
   * return parent::getBaseURL() . 'ServiceName', where 'ServiceName' is
   * a string containing the actual service name implemented by the subclass.
   */
  protected abstract function getURL();

  /**
   * Writes XML document for UPS AccessRequest into output buffer.
   *
   * @return
   *   The UPSXMLWriter object.
   */
  protected function writeHeader() {
    $this->startDocument('1.0', 'UTF-8');
    $this->startElement('AccessRequest');
    $this->writeAttribute('xml:lang', 'en-US');
    $this->writeElement('AccessLicenseNumber', $this->credentials->getAccessLicenseNumber());
    $this->writeElement('UserId', $this->credentials->getUserId());
    $this->writeElement('Password', $this->credentials->getPassword());
    $this->endElement();
    $this->endDocument();
  }

  /**
   * Returns buffer as an XML string.
   */
  public function __toString() {
    return $this->outputMemory(TRUE);
  }
}

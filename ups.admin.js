/**
 * @file
 * Utility functions to display settings summaries on vertical tabs.
 */

(function ($) {

Drupal.behaviors.upsAdminFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-ups-credentials', context).drupalSetSummary(function(context) {
      var server = $('#edit-ups-connection-address :selected', context).text().toLowerCase();
      return Drupal.t('Using UPS @role server', { '@role': server });
    });

    $('fieldset#edit-ups-markups', context).drupalSetSummary(function(context) {
      return Drupal.t('Rate markup') + ': '
        + $('#edit-ups-rate-markup', context).val() + ' '
        + $('#edit-ups-rate-markup-type', context).val() + '<br />'
        + Drupal.t('Weight markup') + ': '
        + $('#edit-ups-weight-markup', context).val() + ' '
        + $('#edit-ups-weight-markup-type', context).val();
    });

    $('fieldset#edit-ups-validation', context).drupalSetSummary(function(context) {
      if ($('#edit-ups-address-validation').is(':checked')) {
        return Drupal.t('Validation is enabled');
      }
      else {
        return Drupal.t('Validation is disabled');
      }
    });

    $('fieldset#edit-ups-quote-options', context).drupalSetSummary(function(context) {
      if ($('#edit-ups-insurance').is(':checked')) {
        return Drupal.t('Packages are insured');
      }
      else {
        return Drupal.t('Packages are not insured');
      }
    });

  }
};

})(jQuery);

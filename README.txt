The UPS module uses the Rate and Services Selection XML Tool to get shipping
rates published by UPS. To use the Online Tools, you will need the following:

* A username and password at http://www.ups.com/.
* A shipper number representing an account with UPS.
* An XML Access Key. To receive this, go to the  Online Tools page at
  http://www.ups.com/e_comm_access/gettools_index and click the "Register" link.
  Apply for a Developer's key which will grant the right to the XML Access Key.

The quote method can request quotes from either the test or production
environment on UPS's servers. Use the testing mode until you are fairly certain
no more errors occur when requesting quotes. The production environment probably
has the most accurate information about shipping rates.

The rest of the settings deal with the way UPS handles your shipments. The more
Ubercart knows about this, the more accurately it can get quotes from UPS.

UPS does not provide all of its services in every location. An error may be
returned when a customer gets a quote and they are not eligible for a service
because of their location.

The "Customer classification" field refers to the kind of customer the store is
to UPS, and should probably match up with the Pickup type.

Negotiated rates, Assuming Residential locations, and the Markup all affect how
much the returned UPS rates will be. The "Product packages" also affects the
shipping price because a single package is cheaper to process and transport
than many boxes of equal total weight.

Ubercart automatically converts the product dimensions into the specified
measurement system when it sends the data to UPS. Choose whichever is commonly
used in your country: inches/pounds for US, metric for nearly everywhere else.
Canada may use either.
